describe('Navegacion', {browser: '!chrome'}, () => {

    it('Navegar a nuestra primer pagina', () => {
        cy.visit('https://platzi.com');
    });

    it('Recargar pagina', () => {
        cy.reload();
    });

    it('Recargar pagina de forma forzada', () => {
        cy.visit('https://www.google.com');
        cy.reload(true);
    });

    it.only('Navegar hacia atras', () => {
        cy.visit('https://www.google.com');
        cy.visit('https://www.google.com/search?q=platzi&source=hp&ei=txnfYonDE5mNwbkPzt2ykAs&iflsig=AJiK0e8AAAAAYt8nxy_BuY5VVFEynoyc7GBxFcBfmf36&gs_ssp=eJzj4tVP1zc0zKiqKMvJzjNQYDRgdGDwYivISSypygQAby4H-g&oq=platzi&gs_lcp=Cgdnd3Mtd2l6EAEYADIRCC4QgAQQsQMQgwEQxwEQ0QMyCwgAEIAEELEDEIMBMgsIABCABBCxAxCDATIFCAAQgAQyBQgAEIAEMgUIABCABDIFCAAQgAQyBQgAEIAEMgUIABCABDIFCAAQgAQ6CAgAELEDEIMBOgsILhCABBCxAxCDAToICAAQgAQQsQM6CwguELEDEMcBEK8BOg4ILhCABBCxAxCDARDUAjoOCC4QsQMQgwEQxwEQrwE6CAguEIAEENQCOgsILhCABBDHARCvAVDTDFinMGC4U2gBcAB4AIABwQGIAagHkgEDMC42mAEAoAEBsAEA&sclient=gws-wiz');
        cy.go("back");
        cy.go("forward");
    });
});