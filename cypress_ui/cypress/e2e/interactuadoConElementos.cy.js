describe('Interactuando con los elementos', () => {
    // para el ejemplo de extraer el texto de un elemento
	let texto

    Cypress.on('uncaught:exception', (err, runnable) => {
        // returning false here prevents Cypress from
        // failing the test
        return false
    })

	it.skip('Click', () => {
		cy.visit('https://demoqa.com/buttons')
		cy.get('button').eq(3).click()
		cy.get('#dynamicClickMessage')
			.should('be.visible')
			.and('contain', 'You have done a dynamic click')
	})

	it.skip('Double Click', () => {
		cy.visit('https://demoqa.com/buttons')
		cy.get('#doubleClickBtn').dblclick()
		cy.get('#doubleClickMessage')
			.should('be.visible')
			.and('contain', 'You have done a double click')
	})

	it.skip('Right Click', () => {
		cy.visit('https://demoqa.com/buttons')
		cy.get('#rightClickBtn').rightclick()
		cy.get('#rightClickMessage')
			.should('be.visible')
			.and('contain', 'You have done a right click')
	})

	it.skip('Force Click', () => {
		cy.visit('https://demoqa.com/dynamic-properties')
		//cy.get('#enableAfter').click({ timeout: 0 })
		cy.get('#enableAfter').click({ timeout: 0, force: true })
	})

	it.skip('Click por posicion', () => {
		cy.visit('https://demoqa.com/buttons')
		cy.get('button').eq(3).parent().parent().click('topRight')
		cy.get('button').eq(3).parent().parent().click('bottomLeft')
		cy.get('button').eq(3).parent().parent().click(5, 60)
	})

	it.skip('Multiple Click', () => {
		cy.visit('https://demoqa.com/buttons')
		cy.get('.btn.btn-primary').click({ multiple: true })
	})

	it.skip('Click con teclas alternativas', () => {
		cy.visit('https://demoqa.com/buttons')
		cy.get('button').eq(3).click({
			shiftKey: true,
			// p optionKey
			altKey: true,
			ctrlKey: true,
			// windows o command en mac
			metaKey: true,
		})
	})

    it.skip('Input type text', () => {
		cy.visit('https://demoqa.com/automation-practice-form')
		cy.get('#firstName').type('Javier')
		cy.get('#lastName').type('Ramírez')

        cy.get('#firstName').type('{selectAll}{backspace}')
		cy.get('#lastName').type('{selectAll}{backspace}')

        cy.get('#firstName').type('Otro Nombre')
		cy.get('#lastName').type('Otro Apellido')

        cy.get('#firstName').clear()
		cy.get('#lastName').clear()
	})

    it.skip('CheckBoxes y Radio Botones', () => {
		cy.visit('https://demoqa.com/automation-practice-form')
        //cy.get('#gender-radio-1"]').click({force: true})
        //cy.get('#gender-radio-1"]').check({force: true})
		cy.get('label[for="gender-radio-1"]').click()

        cy.get('label[for="hobbies-checkbox-1"]').click()
        cy.get('label[for="hobbies-checkbox-1"]').click()
	})

    //Es importante tener el function y no solo un arrow function ay que las arrow function carecen de contexto y por ende del this
	it.skip('Extrayendo informacion', function () {
		cy.visit('https://demoqa.com/automation-practice-form')
		// a veces fallara porque lo cubre otro elemento

		cy.get('#firstName').as('nombre')
		cy.get('@nombre').type('Javier')
		// Primera manera de hacerlo
		cy.get('@nombre').then(($nombre) => {
			texto = $nombre.val()
			expect(texto).to.equal('Javier')
		})

		// Segunda manera de hacerlo, invoke solo invoca una funcion que en este caso el elemento que nos regresa el get , como jquery tiene
		cy.get('@nombre').invoke('val').should('equal', 'Javier')
		cy.get('@nombre').invoke('val').as('nombreGlobal')
	})

    //Es importante tener el function y no solo un arrow function ya que las arrow function carecen de contexto y por ende del this
	it.skip('pasando informacion entre its', function () {
        cy.visit('https://demoqa.com/automation-practice-form')
		// Con la variable global
		cy.get('#lastName').as('nombre2')
        cy.get('@nombre2').type(texto)
		//Con el alias
		cy.get('#firstName').type(this.nombreGlobal)
	})

	it.skip('Interactuado con los dropdown(select)', () =>{

		cy.visit('https://itera-qa.azurewebsites.net/home/automation')

		//Seleccionar por index
		cy.get('.custom-select').select(10)

		//Seleccionar por valor
		cy.get('.custom-select').select('3').should('have.value', '3')

		//Seleccionar por texto
		cy.get('.custom-select').select('Greece').should('have.value', '4')
	})

	it.skip('Interactuando con dropdowns(select) dinamico', () => {
		// Con la variable global
		cy.visit('https://react-select.com/home')

		//Seleccionar por index
		cy.get('#react-select-6-input').type(' ')

		//Iterando por cada uno de los elementos
		cy.get('#react-select-6-listbox')
			.children()
			.children()
			.each( function($el,nombre,list) {
				if ($el.text() === 'Red') {
					//$el.on('click')
					//$el.click
					cy.wrap($el).click()
				}
			})

		//Oh si conoces el id del elemento
		//cy.get('#react-select-6-option-3').click()
	})

	it.skip('Interactuando con tablas', () => {
		// Obteniendo los headers de la tabla
		cy.visit('https://www.w3schools.com/html/html_tables.asp')
		cy.get('#customers')
			.find('th')
			.each(($el, index, $list) => {
				cy.log($el.text())
			})

		cy.get('#customers')
			.find('th')
			.first()
			.invoke('text')
			.should('equal', 'Company')

		cy.get('#customers')
			.find('th')
			.eq(1)
			.invoke('text')
			.should('equal', 'Contact')

		cy.get('#customers')
			.find('th')
			.eq(2)
			.invoke('text')
			.should('equal', 'Country')

		// Validamos el numero de filas
		cy.get('#customers').find('tr').should('have.length', 7)

		cy.get('#customers')
			.find('tr')
			.eq(1)
			.find('td')
			.eq(1)
			.invoke('text')
			.should('equal', 'Maria Anders')

		cy.get('#customers')
			.find('tr')
			.eq(1)
			.find('td')
			.eq(1)
			.then(($el) => {
				const texto = $el.text()
				expect(texto).to.equal('Maria Anders')
				cy.wrap($el).should('contain', 'Maria Anders')
			})
	})

	it.skip('Interactuando con data picker', () => {
		cy.visit('https://material.angular.io/components/datepicker/overview')
		cy.get('datepicker-overview-example')
			.find('input')
			.eq(0)
			.type('12/02/2005{enter}')

		cy.get('datepicker-overview-example').find('svg').click()
	})

	it('Interactuando con modals', () => {
        cy.visit('https://demoqa.com/modal-dialogs')
        cy.get('#showSmallModal').click()
        cy.get('#closeSmallModal').click()
    })

    it.skip('Interactuando con popups', () => {
        cy.visit('https://demoqa.com/alerts')
        //Cypress automaticamente la acepta

        // Primer forma de hacerlo
        /*cy.get('#confirmButton').click()
        cy.on('window:confirm', (confirm) => {
             expect(confirm).to.equal('Do you confirm action?')
     	})
        cy.contains('You selected Ok').should('exist')
	
        // Segundo forma de hacerlo
        const stub = cy.stub()
        cy.on('window:confirm', stub)
        cy.get('#confirmButton').click().then(() => {
            expect(stub.getCall(0)).to.be.calledWith('Do you confirm action?')
        })
        cy.contains('You selected Ok').should('exist')*/


        // rechazar la alerta
        cy.get('#confirmButton').click()
        cy.on('window:confirm', (confirm) => {
             expect(confirm).to.equal('Do you confirm action?')
             return false
        })
        cy.contains('You selected Cancel').should('exist')

    })

    it('Interactuando con tooltips', () => {
        cy.visit('https://demoqa.com/tool-tips')
        cy.get('#toolTipButton').trigger('mouseover')
        cy.contains('You hovered over the Button').should('exist')
        cy.get('#toolTipButton').trigger('mouseout')
        cy.contains('You hovered over the Button').should('not.exist')
    })

	it.only('Interactuando con drag and drops', () => {
        cy.visit('https://demoqa.com/dragabble')
        cy.get('#dragBox')
            .trigger('mousedown', {which: 1, pageX: 600, pageY: 100})
            .trigger('mousemove', {which: 1, pageX: 700, pageY: 400})
            .trigger('mouseup')
    })

})
