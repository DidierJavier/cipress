describe('Guardando elementos',()=>{

    Cypress.on('uncaught:exception', (err, runnable) => {
        // returning false here prevents Cypress from
        // failing the test
        return false
    })

    it('Repeticion',()=>{
        cy.visit('https://demoqa.com/automation-practice-form')
        cy.get('input[placeholder="First Name"]').parents('form').find('label')
        cy.get('input[placeholder="First Name"]').parents('form').find('input')
        cy.get('input[placeholder="First Name"]').parents('form').find('div')
    })

    it('como se hace en cypress',()=>{
        cy.visit('https://demoqa.com/automation-practice-form')
        cy.get('input[placeholder="First Name"]').parents('form').then((form)=>{
            const inputs=form.find('input')
            const divs=form.find('div')
            const labels=form.find('label')

            expect(inputs.length).to.eq(15)

            //esto no servira para cypress modo headless
				console.log('Soy la longitud', inputs.length)

				//usando debugger, es necesario abrir las devtools y debe de ir dentro del then sino probablemente no funcione como deberia
				//debugger
				//cypress log
				cy.log(inputs)

				//cypress task
				cy.task('log', inputs.length)
				//Las aserciones se explicaran a detalle en la proxima clase

            cy.log('Soy la longitud', inputs.length)

            expect(divs.length).to.eq(70)
            expect(labels.length).to.eq(16)
        cy.wrap(inputs).should('have.length',15)
    })
})
})